package mk.ukim.finki.wp.videoaggregator.config;

import mk.ukim.finki.wp.videoaggregator.service.VideoUserDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

/**
 * @author Riste Stojanov
 */
@Configuration
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

  @Autowired
  VideoUserDetailsService userDetailsService;

  @Override
  protected void configure(
    AuthenticationManagerBuilder auth) throws Exception {
    super.configure(auth);
    auth.userDetailsService(userDetailsService)
      .passwordEncoder(passwordEncoder());
  }

  @Override
  protected void configure(HttpSecurity http) throws Exception {
    http
      .csrf().disable()
      .formLogin()
      .loginPage("/login")
      .usernameParameter("email")
      .passwordParameter("password")
      .and()
      .logout()
      .clearAuthentication(true)
      .invalidateHttpSession(true)
      .and()
      .authorizeRequests()
      .antMatchers("/api/*", "/me")
      .hasRole("Data user")
      .and()
      .authorizeRequests()
      .antMatchers("/*").permitAll();
  }

  @Bean
  public PasswordEncoder passwordEncoder() {
    BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
    return encoder;
  }
}
