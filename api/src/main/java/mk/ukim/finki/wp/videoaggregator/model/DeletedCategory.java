package mk.ukim.finki.wp.videoaggregator.model;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.io.Serializable;

/**
 * @author Riste Stojanov
 */
@Entity
@Table(name = "categories")
@Where(clause = "deleted=true")
public class DeletedCategory implements Serializable {

  @Id
  @GeneratedValue
  public Long id;

  public String title;

  public boolean deleted = false;

  @Override
  public int hashCode() {
    return id != null ? id.hashCode() : 0;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;

    DeletedCategory category = (DeletedCategory) o;

    return id != null ? id.equals(category.id) : category.id == null;
  }
}
