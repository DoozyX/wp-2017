package mk.ukim.finki.wp.videoaggregator.model;

/**
 * @author Riste Stojanov
 */
public enum Role {

  ROLE_API, ROLE_PUBLIC_WEB, ROLE_WEB_USER
}
