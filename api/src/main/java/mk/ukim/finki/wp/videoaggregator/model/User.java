package mk.ukim.finki.wp.videoaggregator.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * @author Riste Stojanov
 */
@Document(collection = "users")
public class User {

  @Id
  public String username;

  public String password;

  public String email;

  public Role role;


  @Override
  public String toString() {
    final StringBuffer sb = new StringBuffer("User{");
    sb.append("username='").append(username).append('\'');
    sb.append(", email='").append(email).append('\'');
    sb.append('}');
    return sb.toString();
  }
}
