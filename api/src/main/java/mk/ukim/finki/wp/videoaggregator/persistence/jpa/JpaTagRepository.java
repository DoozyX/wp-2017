package mk.ukim.finki.wp.videoaggregator.persistence.jpa;

import mk.ukim.finki.wp.videoaggregator.model.Tag;
import mk.ukim.finki.wp.videoaggregator.persistence.TagRepository;
import org.springframework.context.annotation.Profile;
import org.springframework.data.repository.Repository;

/**
 * @author Riste Stojanov
 */
@Profile("jpa")
public interface JpaTagRepository extends TagRepository,
  Repository<Tag, String> {
}
