package mk.ukim.finki.wp.videoaggregator.persistence.jpa;

import mk.ukim.finki.wp.videoaggregator.model.Video;
import mk.ukim.finki.wp.videoaggregator.persistence.VideoRepository;
import org.springframework.context.annotation.Profile;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.Repository;
import org.springframework.data.repository.query.Param;

import java.util.Optional;

/**
 * @author Riste Stojanov
 */
@Profile("jpa")
public interface JpaVideoRepository extends VideoRepository, Repository<Video, Long> {

  @EntityGraph(type = EntityGraph.EntityGraphType.FETCH,
    attributePaths = {"tags"})
  @Query("SELECT v FROM #{#entityName} v WHERE v.id = ?1")
  Optional<Video> findOneVideoWithTags(Long id);

  @Override
//  @Query(value = "SELECT v from Video v WHERE v.category.title=?1")
  @Query(value = "SELECT v.* from videos v " +
    "JOIN categories c ON c.id=v.my_category_id WHERE c.title=?1",
    nativeQuery = true)
  Iterable<Video> findByCategoryTitle(@Param("title") String title);

  @EntityGraph(type = EntityGraph.EntityGraphType.FETCH,
    attributePaths = {"category", "tags"})
  @Query("SELECT v FROM #{#entityName} v")
  Iterable<Video> findAllFetchingAllProperties();

  Page<Video> findAll(Specification<Video> specification,
                      Pageable pageable);
}
