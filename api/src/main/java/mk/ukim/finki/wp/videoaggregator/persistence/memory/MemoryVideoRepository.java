package mk.ukim.finki.wp.videoaggregator.persistence.memory;

import mk.ukim.finki.wp.videoaggregator.model.Video;
import mk.ukim.finki.wp.videoaggregator.persistence.VideoRepository;
import org.springframework.context.annotation.Profile;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

/**
 * @author Riste Stojanov
 */
@Deprecated
@Profile("memory")
@Repository
public class MemoryVideoRepository implements VideoRepository {

  private static Long idSequence = 0L;
  private Map<Long, Video> mapDb = new HashMap<>();

  @Override
  public Video save(Video video) {
    Video newVideo = new Video();
    idSequence++;
    newVideo.id = idSequence;
    newVideo.title = video.title;
    newVideo.description = video.description;
    newVideo.url = video.url;
    newVideo.category = video.category;
    newVideo.tags = video.tags;

    return mapDb.put(newVideo.id, newVideo);
  }

  @Override
  public Optional<Video> findOne(Long id) {
    return Optional.ofNullable(mapDb.get(id));
  }

  @Override
  public Optional<Video> findOneVideoWithTags(Long id) {
    return Optional.ofNullable(mapDb.get(id));
  }

  @Override
  public void delete(Video video) {
    mapDb.remove(video.id);
  }

  @Override
  public Iterable<Video> findByCategoryTitle(String title) {
    return null;
  }

  @Override
  public Iterable<Video> findAll() {
    return null;
  }

  @Override
  public Page<Video> findAll(Pageable pageable) {
    return null;
  }

  @Override
  public Iterable<Video> findAllFetchingAllProperties() {
    return null;
  }

  @Override
  public Iterable<Video> findByTagsName(String tagName) {
    return null;
  }

  @Override
  public Iterable<Video> findByTitleIgnoreCaseContaining(String title) {
    return null;
  }

  @Override
  public Page<Video> findAll(Example<Video> example, Pageable pageable) {
    return null;
  }
}
