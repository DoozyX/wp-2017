package mk.ukim.finki.wp.videoaggregator.service;

import mk.ukim.finki.wp.videoaggregator.model.Video;

import java.util.List;

/**
 * @author Riste Stojanov
 */
public interface SearchService {

  List<Video> searchVideo(String phrase);
}
