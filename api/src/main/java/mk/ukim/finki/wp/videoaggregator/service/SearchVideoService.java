package mk.ukim.finki.wp.videoaggregator.service;

import mk.ukim.finki.wp.videoaggregator.model.Video;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import java.util.Optional;

/**
 * @author Riste Stojanov
 */
public interface SearchVideoService {

  Optional<Video> findByIdWithoutTags(Long id);

  Optional<Video> findByIdWithTags(Long id);

  Page<Video> findAll(Pageable pageable);



  Iterable<Video> findByCategoryTitle(String categoryTitle);

  Iterable<Video> findByTagName(String tagName);

  Page<Video> findByTitle(Pageable pageable, String title);

}
