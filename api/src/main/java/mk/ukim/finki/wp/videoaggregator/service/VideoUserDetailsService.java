package mk.ukim.finki.wp.videoaggregator.service;

import mk.ukim.finki.wp.videoaggregator.model.User;
import mk.ukim.finki.wp.videoaggregator.persistence.mongo.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.stream.Stream;

import static java.util.stream.Collectors.toList;

/**
 * @author Riste Stojanov
 */
@Service
public class VideoUserDetailsService implements UserDetailsService {

  private final UserRepository userRepository;

  @Autowired
  public VideoUserDetailsService(UserRepository userRepository) {
    this.userRepository = userRepository;
  }

  @Override
  public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
    User user= userRepository.findByUsername(username);

    return new org.springframework.security.core.userdetails.User(
      user.username,
      user.password,
      Stream
        .of(new SimpleGrantedAuthority(user.role.toString()))
        .collect(toList())
    );
  }
}
