package mk.ukim.finki.wp.videoaggregator.web.controllers;

import mk.ukim.finki.wp.videoaggregator.service.LastNameProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

/**
 * @author Riste Stojanov
 */
@Controller
public class HelloController {

    private LastNameProvider provider;

    @Autowired
    public HelloController(LastNameProvider provider) {
        this.provider = provider;
    }

    @GetMapping(value = "/hello_controller")
    public String hello() {
        return "index";
    }

    @GetMapping(value = "/hello_to")
    public ModelAndView hello(@RequestParam(defaultValue = "Riste") String name) {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("index");
        modelAndView.addObject("name", name);
        return modelAndView;
    }

    @RequestMapping(value = "/hello_to/{name}", method = RequestMethod.GET)
    public ModelAndView helloTo(@PathVariable String name) {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("index");
        modelAndView.addObject("name", provider.lastName(name));
        return modelAndView;
    }


    @RequestMapping(value = "/hello_invalid", method = RequestMethod.GET)
    public String helloInvalid() {
        return "invalid";
    }
}
