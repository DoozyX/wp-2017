package mk.ukim.finki.wp.videoaggregator.web.controllers;

import mk.ukim.finki.wp.videoaggregator.model.Video;
import mk.ukim.finki.wp.videoaggregator.service.SearchService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

import static mk.ukim.finki.wp.videoaggregator.web.controllers.LandingController.buildURIFromParams;

/**
 * @author Riste Stojanov
 */
@Controller
public class SearchController {


  private final SearchService searchService;

  @Autowired
  public SearchController(SearchService searchRepository) {
    this.searchService = searchRepository;
  }

  @GetMapping("/search")
  public String search(ModelMap model, @RequestParam String query) {
    List<Video> res = searchService.searchVideo(query);
//    model.addAttribute("prevPageRequest", buildURIFromParams(query, 0, res.size()));
//    model.addAttribute("nextPageRequest", buildURIFromParams(query, 0, res.size());
    model.addAttribute("query", query);
    model.addAttribute("videos", res);
    model.addAttribute("pageNumber", 0);
    model.addAttribute("prevPage", 0);
    model.addAttribute("nextPage", 0);
    model.addAttribute("hasNext", false);
    model.addAttribute("hasPrev", false);

    return "fragments/contents";
  }
}
