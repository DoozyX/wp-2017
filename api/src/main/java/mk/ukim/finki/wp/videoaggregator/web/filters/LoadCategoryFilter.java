package mk.ukim.finki.wp.videoaggregator.web.filters;

import mk.ukim.finki.wp.videoaggregator.model.Category;
import mk.ukim.finki.wp.videoaggregator.persistence.CategoryRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * @author Riste Stojanov
 */
@Component
public class LoadCategoryFilter implements Filter {


  private static final String CATEGORIES = "categories";
  private static Logger logger = LoggerFactory.getLogger(LoadCategoryFilter.class);


  private final CategoryRepository categoryRepository;

  @Autowired
  public LoadCategoryFilter(CategoryRepository categoryRepository) {
    this.categoryRepository = categoryRepository;
  }


  @Override
  public void init(FilterConfig filterConfig) throws ServletException {

  }

  @Override
  public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
    HttpServletRequest httpRequest = (HttpServletRequest) request;
    HttpSession session = httpRequest.getSession();
    logger.debug("Loading categories in filter from DB");
    Iterable<Category> categories = categoryRepository.findAll();
    httpRequest.setAttribute(CATEGORIES, categories);
    session.setAttribute(CATEGORIES, categories);
    chain.doFilter(request, response);

  }

  @Override
  public void destroy() {

  }
}
