package mk.ukim.finki.wp.videoaggregator.web.j2ee;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author Riste Stojanov
 */

@WebServlet("/hello")
public class HelloWorldServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        try (ServletOutputStream out = resp.getOutputStream()) {
            System.out.println("In servlet");
            out.print("<html>");
            out.print("<head>");
            out.print("</head>");
            out.print("<body>");
            out.print("<h1>");
            out.print("Hello ");
            out.print(req.getParameter("name"));
            out.print(" ");
            out.print(req.getParameter("lastName"));
            out.print("</h1>");
            out.print("<br/>");

            out.print((String) req.getSession().getAttribute("browser"));
            out.print("<hr/>");
            out.print((String) req.getAttribute("ip"));
//      out.print(req.getSession().getAttribute(""));
            out.print("</body>");
            out.print("</html>");
        }
    }
}
