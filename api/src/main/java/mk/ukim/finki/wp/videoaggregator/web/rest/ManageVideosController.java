package mk.ukim.finki.wp.videoaggregator.web.rest;

import mk.ukim.finki.wp.videoaggregator.model.Tag;
import mk.ukim.finki.wp.videoaggregator.model.Video;
import mk.ukim.finki.wp.videoaggregator.service.ManageVideoService;
import mk.ukim.finki.wp.videoaggregator.service.VideoUrlValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author Riste Stojanov
 */

@CrossOrigin("http://localhost:4200")
@RestController
@RequestMapping(value = "/api", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
public class ManageVideosController {

    private final ManageVideoService service;
    private final VideoUrlValidator videoUrlValidator;

    @Autowired
    public ManageVideosController(ManageVideoService service,
                                  VideoUrlValidator videoUrlValidator) {
        this.service = service;
        this.videoUrlValidator = videoUrlValidator;
    }

    @RequestMapping(value = "videos", method = RequestMethod.POST)
    public Video create(@RequestParam String title, @RequestParam String url,
                        @RequestParam String description, @RequestParam Long categoryId) {
        return service.validateUrlAndCreateVideo(title, url, description, categoryId);
    }

    @RequestMapping(value = "jsonVideos", method = RequestMethod.POST)
    public Video create(@RequestBody Video video) {
      if(video.id == null){
        return service.validateUrlAndCreateVideo(
          video.title,
          video.url,
          video.description,
          video.category.id);
      }
      else{
        service.updateVideo(video.id, video.title, video.description);
        return null;
      }
    }

    @RequestMapping(value = "videos/{id}", method = RequestMethod.PATCH)
    public void updateVideo(@PathVariable Long id, @RequestParam String newTitle,
                            @RequestParam String newDescription
    ) {
        service.updateVideo(id, newTitle, newDescription);
    }

    @RequestMapping(value = "videos/{id}", method = RequestMethod.DELETE)
    public void removeVideo(@PathVariable Long id) {
        service.removeVideo(id);
    }

    @RequestMapping(value = "videos/{id}/add-tag", method = RequestMethod.PATCH)
    public void addTagToVideo(@PathVariable("id") Long videoId, @RequestParam String tag) {
        service.addTagToVideo(videoId, tag);
    }

    @RequestMapping(value = "videos/{id}/remove-tag", method = RequestMethod.PATCH)
    public void removeTagFromVideo(@PathVariable("id") Long videoId, @RequestParam String tag) {
        service.removeTagFromVideo(videoId, tag);
    }

    @RequestMapping(value = "videos/{id}/category", method = RequestMethod.PATCH)
    public void updateVideoCategory(@PathVariable("id") Long videoId, Long categoryId) {
        service.updateVideoCategory(videoId, categoryId);
    }

    @RequestMapping(value = "/servlet", method = RequestMethod.GET)
    public void simulateServlet(HttpServletRequest req, HttpServletResponse resp) {

    }
}
