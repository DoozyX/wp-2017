package mk.ukim.finki.wp.videoaggregator;

import com.jayway.restassured.http.ContentType;
import mk.ukim.finki.wp.videoaggregator.model.Category;
import mk.ukim.finki.wp.videoaggregator.persistence.CategoryRepository;
import org.apache.http.HttpStatus;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;

import static com.jayway.restassured.RestAssured.given;
import static org.hamcrest.Matchers.*;

@ActiveProfiles({"test", "jpa"})
public class CategoryResourceTest extends BaseIntegrationTest {


  @Autowired
  CategoryRepository categoryRepository;


  Category category;

  @BeforeClass
  public void setUp() {
    category = new Category();
    category.id = 1L;
    category.title = "test";
    category = categoryRepository.save(category);
    System.out.println(category);
  }


  @AfterClass
  public void cleanUp() {
    categoryRepository.delete(1L);
  }


  @Test
  public void test_initial_empty_categories() {
    given().accept(ContentType.JSON)
      .when()
      .log().all()
      .get("/api/categories/{id}", 1L)
      .then()
      .log().all()
      .assertThat()
      .statusCode(HttpStatus.SC_OK)
      .contentType(ContentType.JSON)
      .body(is(not(isEmptyOrNullString())));
  }

  @Test
  public void test_find_by_title() {
    given().accept(ContentType.JSON)
      .when()
      .log().all()
      .param("title", "test")
      .get("/api/categories/by_title")
      .then()
      .log().all()
      .assertThat()
      .statusCode(HttpStatus.SC_OK)
      .contentType(ContentType.JSON)
      .body(is(not(isEmptyOrNullString())));
  }

  @Test
  public void test_find_by_title_empty() {
    given().accept(ContentType.JSON)
      .when()
      .log().all()
      .param("title", "riste")
      .get("/api/categories/by_title")
      .then()
      .log().all()
      .assertThat()
      .statusCode(HttpStatus.SC_OK)
      .contentType(ContentType.JSON)
      // https://stackoverflow.com/questions/30460682/assert-that-response-body-is-empty-list-with-rest-assured?answertab=votes#tab-top
      .body("$", hasSize(0));
  }

  @Test
  public void add_category() {
    Category category = new Category();
    category.title = "test od cas";

    given().accept(ContentType.JSON)
      .when()
      .log().all()
      .body(category)
      .contentType(ContentType.JSON)
      .post("/api/categories")
      .then()
      .log().all()
      .assertThat()
      .statusCode(HttpStatus.SC_CREATED)
      .contentType(ContentType.JSON)
      .body("title", is("test od cas"))
      .body("_links.category.href", not(empty()))
    ;
  }


  @Test
  public void update_category() {
    Category category = new Category();
    category.id = 1L;
    category.title = "updated na cas";

    given().accept(ContentType.JSON)
      .when()
      .log().all()
      .body(category)
      .contentType(ContentType.JSON)
      .put("/api/categories/{id}", 1L)
      .then()
      .log().all()
      .assertThat()
      .statusCode(HttpStatus.SC_OK)
      .contentType(ContentType.JSON)
      .body("title", is("updated na cas"))
      .body("id", not(empty()))
    ;
  }
}
