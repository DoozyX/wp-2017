package mk.ukim.finki.wp.videoaggregator;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class VideoAgregatorApplicationTests {

    @Test
    public void contextLoads() {
    }

}
