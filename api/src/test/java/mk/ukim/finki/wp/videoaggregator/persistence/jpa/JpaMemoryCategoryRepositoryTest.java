package mk.ukim.finki.wp.videoaggregator.persistence.jpa;

import mk.ukim.finki.wp.videoaggregator.model.Category;
import mk.ukim.finki.wp.videoaggregator.model.Tag;
import mk.ukim.finki.wp.videoaggregator.model.User;
import mk.ukim.finki.wp.videoaggregator.model.Video;
import mk.ukim.finki.wp.videoaggregator.persistence.TagRepository;
import mk.ukim.finki.wp.videoaggregator.persistence.mongo.UserRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.data.mongo.DataMongoTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.*;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.domain.Specifications;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Path;
import java.util.function.Consumer;

/**
 * @author Riste Stojanov
 */
@RunWith(SpringRunner.class)
@DataJpaTest
@DataMongoTest
@ActiveProfiles({"test", "jpa"})
public class JpaMemoryCategoryRepositoryTest {

  private static Logger logger = LoggerFactory.getLogger(JpaMemoryCategoryRepositoryTest.class);


  private static Consumer<Video> videoPrintingConsumerWithoutTags = v -> {
    logger.debug("\tvideo id: {}", v.id);
    logger.debug("\tvideo title: {}", v.title);
    logger.debug("\tvideo category title: {}", v.category.title);

  };

  private static Consumer<Video> videoPrintingConsumer = v -> {
    videoPrintingConsumerWithoutTags.accept(v);
    logger.debug("\tvideo tags size: {}", v.tags.size());

  };


  @Autowired
  private UserRepository userRepository;

  @Autowired
  private JpaCategoryRepository repository;

  @Autowired
  private TagRepository tagRepository;

  @Autowired
  private JpaVideoRepository videoRepository;

  @PersistenceContext
  private EntityManager em;


  @Before
  public void setUp() {
    Category c1 = new Category();
    c1.title = "c1";
    Category c2 = new Category();
    c2.title = "c2";
    Category c3 = new Category();
    c3.title = "c3";
    repository.save(c1);
    repository.save(c2);
    repository.save(c3);

    Tag t1 = new Tag();
    t1.name = "t1";
    Tag t2 = new Tag();
    t2.name = "t2";
    Tag t3 = new Tag();
    t3.name = "t3";
    Tag t4 = new Tag();
    t4.name = "t4";


    Video v1 = new Video();
    v1.title = "v1";
    v1.category = c1;
    v1.tags.add(t1);


    Video v2 = new Video();
    v2.title = "v2";
    v2.category = c2;
    v2.tags.add(t3);
    v2.tags.add(t4);


    Video v3 = new Video();
    v3.title = "v3";
    v3.category = c3;
    v3.tags.add(t1);
    v3.tags.add(t2);
    v3.tags.add(t4);

    videoRepository.save(v1);
    videoRepository.save(v2);
    videoRepository.save(v3);

    for (int i = 0; i < 10; i++) {
      Video vi = new Video();
      vi.title = "v" + i;
      vi.category = c1;
      videoRepository.save(vi);
    }

    em.clear();
  }


  @Test
  public void testFindAllVideos() {
    logger.debug("start find all videos");
    Iterable<Video> res = videoRepository.findAll();
    res.forEach(videoPrintingConsumer);
    logger.debug("end find all videos");
  }

  @Test
  public void testFetchAllVideos() {
    logger.debug("start find all videos");
    Iterable<Video> res = videoRepository.findAllFetchingAllProperties();
    res.forEach(videoPrintingConsumer);
    logger.debug("end find all videos");
  }


  @Test
  public void testFindVideosWithSpecification() {
    logger.debug("start find videos with specification");
    Pageable pageable = getPageable(0, 4, "title");

    Specifications spec = simulateUserFiltering();

    spec = spec.and(getSecurityFilter());

    Iterable<Video> res = videoRepository.findAll(spec,
      pageable);
    res.forEach(videoPrintingConsumer);
    logger.debug("end find videos with specification");
  }

  private Specifications simulateUserFiltering() {

    String startsWithV = "v%";
    String contains1 = "%1%";
    Specification<Video> byTitleLike = (root, query, cb) -> {
      Path<String> videoTitle = root.get("title");
      return cb.like(videoTitle, startsWithV);
    };

    Specification<Video> byCategoryTitleLike =
      (root, query, cb) -> cb.like(root.join("category", JoinType.LEFT).get("title"), contains1
      );

    Specifications spec = Specifications.where(null);
    int random = (int) (System.nanoTime() % 4);
    switch (random) {
      case 1:
        logger.debug("Searching by video title only");
        spec = spec.and(byTitleLike);
        break;
      case 2:
        logger.debug("Searching by category title only");
        spec = spec.and(byCategoryTitleLike);
        break;
      case 3:
        logger.debug("Searching by video title and category title");
        spec = spec
          .and(byTitleLike)
          .and(byCategoryTitleLike);
        break;
    }
    return spec;
  }

  @Test
  public void testFetchPage() {
    logger.debug("start find all videos");
    Pageable pageable = getPageable(0, 4, "title");
    Page<Video> res = videoRepository.findAll(pageable);
    logger.debug("total pages: {}; total elements: {}",
      res.getTotalPages(),
      res.getTotalElements()
    );
    res.forEach(videoPrintingConsumerWithoutTags);

    while (res.hasNext()) {
      Pageable next = res.nextPageable();
      res = videoRepository.findAll(next);
      logger.debug("total pages: {}; total elements: {}",
        res.getTotalPages(),
        res.getTotalElements()
      );
      res.forEach(videoPrintingConsumerWithoutTags);
    }


    logger.debug("end find all videos");
  }


  @Test
  public void testSearchByExample() {
    logger.debug("start find videos by example");
    Pageable pageable = getPageable(0, 4, "title");

    Video video = new Video();
    video.title = "";


    ExampleMatcher matcher = ExampleMatcher.matching()
      .withIgnoreNullValues()
      .withStringMatcher(ExampleMatcher.StringMatcher.ENDING);

    Example<Video> videoExample = Example.of(video, matcher);

    Page<Video> res = videoRepository.findAll(videoExample, getPageable(0, 4, "title"));
    logger.debug("total pages: {}; total elements: {}",
      res.getTotalPages(),
      res.getTotalElements()
    );
    res.forEach(videoPrintingConsumerWithoutTags);
    logger.debug("end find videos by example");
  }

  @Test
  public void createUserAndThanFetchByUsername() {
    User user = new User();
    user.username = "test";
    user.email = "test@test.com";

    user = userRepository.save(user);
    logger.warn("saved: {}", user);

    user = userRepository.findOne("test");

    logger.warn("fetched: {}", user);

  }


  @Test
  public void testSave() {
    Category category = new Category();
    category.title = "test";
    repository.save(category);

  }

  @Test(expected = DataIntegrityViolationException.class)
  public void testDeleteCategory() {
    logger.debug("deleting category with id 1");
    repository.delete(1);
    logger.debug("finished deleting category 1");

  }


  @Test
  public void testSoftDeleteCategory() {
    long count = repository.count();

    logger.debug("deleting category with id 1. There are {} categories.", count);
    repository.softDelete(1);
    count = repository.count();
    logger.debug("finished deleting category 1. There are {} categories.", count);


  }

  private Pageable getPageable(int page, int size, String sortColumn) {
    return new PageRequest(page,
      size,
      new Sort(Sort.Direction.ASC, sortColumn)
    );
  }

  public Specification getSecurityFilter() {
    return null;
  }
}
