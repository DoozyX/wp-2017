package mk.ukim.finki.wp.videoaggregator.service.impl

import mk.ukim.finki.wp.videoaggregator.model.Category
import mk.ukim.finki.wp.videoaggregator.model.Video
import mk.ukim.finki.wp.videoaggregator.model.exceptions.InvalidCategory
import mk.ukim.finki.wp.videoaggregator.persistence.CategoryRepository
import mk.ukim.finki.wp.videoaggregator.persistence.VideoRepository
import mk.ukim.finki.wp.videoaggregator.service.VideoUrlValidator
import spock.lang.Specification

class ManageVideoServiceImplSpec
  extends Specification {
  VideoRepository mockVideoRepository
  CategoryRepository mockCategoryRepository
  VideoUrlValidator mockVideoUrlValidator

  ManageVideoServiceImpl manageVideoService

  def setup() {
    mockVideoRepository = Mock()
    mockCategoryRepository = Mock()
    mockVideoUrlValidator = Mock()
    manageVideoService =
      new ManageVideoServiceImpl(mockVideoRepository, null, mockCategoryRepository, mockVideoUrlValidator)
  }

  def "should fail creating video with non existing category"() {
    given:
    def categoryId = -1 // non existing
    mockCategoryRepository.findOne(categoryId) >> Optional.empty()

    when:
    manageVideoService.create("title", "url", "desc", categoryId)

    then:
    InvalidCategory ex = thrown(InvalidCategory)
    ex != null
    ex.message != null
  }

  def "should create video"() {
    given:
    def category = new Category()
    category.id = 1
    category.title = "test category"

    def video = new Video()
    video.id = 1
    video.title = "title"
    video.url = "url"
    video.description = "desc"
    video.category = category

    mockCategoryRepository.findOne(1) >> Optional.of(category)
    mockVideoRepository.save(_ as Video) >> video

    when:
    def actual = manageVideoService.create("title", "url", "desc", category.id)

    then:
    actual.category != null
    actual.description == "desc"
    actual.title == "title"
    actual.url == "url"
  }

}
