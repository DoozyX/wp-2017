import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {VideoDetailsComponent} from './video-details/video-details.component';
import {VideoEditComponent} from './video-edit/video-edit.component';

const routes: Routes = [
  {path: '', redirectTo: 'edit/', pathMatch: 'full'},
  {path: 'view/:title', component: VideoDetailsComponent},
  {path: 'edit/:title', component: VideoEditComponent},
  {path: 'edit/', component: VideoEditComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
