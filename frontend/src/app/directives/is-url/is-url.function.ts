import {AbstractControl, ValidatorFn} from '@angular/forms';

export function isUrl(): ValidatorFn {
  // The input argument of the function is AbstractControl
  // It returns a key-value pairs as an object
  return (control: AbstractControl): { [key: string]: any } => {
    // get the value from the control
    const value = control.value;
    // Create regular expression that will validate the url. Source:
    // https://stackoverflow.com/questions/13820477/html5-input-tag-validation-for-url?answertab=votes#tab-top
    const html = new RegExp('^(([^:/?#]+):)?(//([^/?#]*))?([^?#]*)(\\?([^#]*))?(#(.*))?');
    // Process the url
    // It returns an array of the elements in the small brackets if there is a match, or null otherwise
    const matches = html.exec(value);


    // if there is a match, and the domain is youtube.com, no validation error is present
    if (matches && matches[4] === 'youtube.com') {
      return null;
    } else {
      console.log('invalid value: ', value);
      // return the validation error
      return {
        'isUrl': {
          value: value,
          invalidDomain: matches && matches[4]

        }
      };
    }
  };
}
