import {Component, Input, OnInit} from '@angular/core';
import {Video} from '../../model/Video';
import {VideoServiceInterface} from "../services/VideoServiceInterface";
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-video-edit',
  templateUrl: './video-edit.component.html',
  styleUrls: ['./video-edit.component.css']
})
export class VideoEditComponent implements OnInit {

  public CREATE_ACTION = 'Create';
  public EDIT_ACTION = 'Edit';

  private _editingVideo: Video;

  protected categories;


  private action = this.CREATE_ACTION;
  private isEdit = false;
  private newTag: string;

  protected video: Video;
  private videoTitle;

  @Input('editingVideo')
  set setEditingVideo(editingVideo: Video) {

    console.log('Set editing video');
    this.setVideo(editingVideo);
  }


  constructor(private route: ActivatedRoute,
              private  videoService: VideoServiceInterface) {
    this.video = new Video();
  }

  private setVideo(editingVideo: Video) {
    console.log('Setting video');
    // if the editingVideo is not set, we should prevent the error
    if (editingVideo) {
      this.action = this.EDIT_ACTION;
      this.isEdit = true;
      this._editingVideo = editingVideo;
      this.video.title = editingVideo.title;
      this.video.description = editingVideo.description;
      this.video.tags = editingVideo.tags;
      this.video.course = editingVideo.course;
      this.video.url = editingVideo.url;
      this.video.id = editingVideo.id;
    }
  }


  ngOnInit() {

    this.videoTitle = this.route.snapshot.paramMap.get('title');

    this.videoService.loadCategories()
      .subscribe(categories => this.categories = categories);

    if (this.videoTitle) {
      console.info('title', this.videoTitle);
      this.loadVideo(this.videoTitle);
    }
  }

  public save(): void {
    console.log('saving...');
    this.videoService.save(this.video)
      .subscribe(videoFromServer => this.setVideo(videoFromServer));
    // we should reset the video instance after saving it
    this.video = new Video();
  }

  public edit() {
    this.videoService.edit(this._editingVideo, this.video)
      .subscribe(videoFromServer => this.loadVideo(this.videoTitle));
  }

  public removeTag(tag: string) {
    console.log(this.video)
    this.videoService.removeTag(this.video.id, tag);
  }

  public addTag(tagName: string) {
    this.videoService.addTag(this.video.id, this.newTag)
      .subscribe(() => this.newTag = null);

  }


  private loadVideo(videoTitle: string) {
    this.videoService.findByTitle(videoTitle)
      .subscribe(videos => {
          if (videos.length > 0) {
            console.info('edititng video', videos);
            this.setVideo(videos[0]);
          }
        },
        error => {
          console.error(error.errorMessage);
        }
      );
  }


}
